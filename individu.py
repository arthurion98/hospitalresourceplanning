import random as rd
import numpy as np
import pygame
import hopital
import numpy as np
from vars_importantes import *


class Individu():

    def __init__(self, region, grille_size, pos, size, succept=False, etat=Etat.Sain, soigne=False, bienSoigne=False):
        """
        :param grille: (Grille) grille qui contient l'individu
        :param pos: (tuple (x, y)) position initiale de l'individu dans la grille
        :param size: (tuple (width, height)) taille de l'individu dans la grille
        :param etat: (Etat) état de l'individu
        :param succept (Succept) succeptibilité à être infecté
        :param soin (bolléan) indique s'il est soigné à l'hopital ou non

        :param hopital (pointeur) pointe dur l'hopital intialisé avec soin
        """
        self.region = region
        self.pos = pos
        self.x = pos[0] + 2
        self.y = pos[1] + 2
        self.size = size
        self.rect = (self.x, self.y, 2 * self.size[0], 2 * self.size[1])
        self.grille_size = grille_size
        self.grille_w = grille_size[0]
        self.grille_h = grille_size[1]
        self.etat = etat
        self.succept = succept
        self.soigne = soigne
        self.bienSoigne = bienSoigne
        self.eteSoigne= False
        self.init_proba(probabilites,lam)
        self.inGrille = True

    def collide_body(self, pos):
        if np.abs(self.x - pos[0]) < self.size[0] and np.abs(self.y - pos[1]) < self.size[1]:
            return True
        return False

    def collide_contamination(self, pos, rayon):
        if np.sqrt((self.x - pos[0]) ** 2 + (self.y - pos[1]) ** 2) < rayon:
            return True
        return False

    def update_pos(self, pos):
        self.x = min(max(pos[0], 2), self.grille_w + 2)
        self.y = min(max(pos[1], 2), self.grille_h + 2)
        self.pos = (self.x, self.y)
        self.rect = (self.x, self.y, 2 * self.size[0], 2 * self.size[1])


    def init_proba(self,probas,lambda_):
        self.probas=probas[self.succept]
        self.Lambda=lambda_[self.succept]


    def contamine(self):
        self.temps = 0
        self.etat = Etat.IIncub

    def draw(self, win, pos_grid):
        pygame.draw.rect(win, couleurs[couleurs_etats[etats_abr[self.etat]]], (self.rect[0] + pos_grid[0], self.rect[1] + pos_grid[1], self.rect[2], self.rect[3]))



    def update(self):
        if self.etat in porteur:
            self.temps += 1
            self.update_self()

    def estMort(self):
        return (self.etat == Etat.Mort)

    def demande_hopital(self):
        return ((not self.soigne) and (self.etat in malade)) #vu que soigne est pas reset au retour pour les stats, le système est concu pour soigne -> immunise

    def estSoigne(self):
        return ((not self.inGrille) and (self.etat == Etat.Immunise)) #indique si est hors de la grille -> hosto et immunise

    def update_self(self):
        if (not self.soigne):
            Lam = self.Lambda[0]
            Proba = self.probas[0]
        else:
            if (not self.bienSoigne):
                Lam = self.Lambda[1]
                Proba= self.probas[1]
            else:
                Lam = self.Lambda[2]
                Proba= self.probas[2]
                print(Lam)

        R = rd.random()
        R2 = rd.random()
        #si l'etat est sain ou immunisé, pas d'update

        #si l'etat est incube, peut soit changer d'état (à IAS ou ISF) soit non
        if self.etat == Etat.IIncub:
            #calcule la probabilité de changer
            #print("{} < {}".format(R,self.proba_exp(Lam[0])))
            if R<self.proba_exp(Lam[0]):
                if R<Proba[0]:
                    self.etat = Etat.IASymp
                    self.temps=0
                else:
                    self.etat = Etat.ISympF
                    self.temps=0

        elif self.etat == Etat.IASymp:
            if R<self.proba_exp(Lam[1]):
                self.etat = Etat.Immunise

        elif self.etat == Etat.ISympF:
            #proba qu'il change d'état en fonction de son nombre de jours
            if R<self.proba_exp(Lam[2]):
                self.etat = Etat.ISympM
                self.temps= 0
            else:
                if R2<Proba[1]:
                    self.etat= Etat.Immunise
                #sinon il ne change pas son état

        elif self.etat == Etat.ISympM:
            #proba qu'il change d'état en fonction de son nombre de jours
            if R<self.proba_exp(Lam[3]):
                self.etat = Etat.ISympG
                self.temps=0
            else:
                if R2<Proba[2]:
                    self.etat= Etat.Immunise
                #sinon il ne change pas son état

        elif self.etat == Etat.ISympG:
            #proba qu'il change d'état en fonction de son nombre de jours
            if R<self.proba_exp(Lam[4]):
                self.etat = Etat.Mort
            else:
                if R2<Proba[3]:
                    self.etat= Etat.Immunise
                #sinon il ne change pas son état

    def proba_exp(self, Lambda):
        return 1- np.exp(-1*Lambda*self.temps)







