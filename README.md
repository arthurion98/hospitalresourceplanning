READ ME - Project team 254 - April 2020


## Introduction
    This is a project created to simulate the development of a pandemic (here covid-19) and look at how the hospitals can manage the situation.
    The goal of the project is to creat a modulable tool that can be used and modified to match different variables and scenarios
    and thus to make hypothetical predictions in different cases.

## Running
    The simulation will create some individuals inside a closed region,
    simulate a spread of the disease within the population
    and the individuals with symptoms will be transferred to the hospitals in their region.
    The hospitals will treat more or less well the patients (depending on their medical staff and other resources)
    and the patients will then recover and become immune or die of the disease.

    Infected individuals incubate the pathogen for a certain period of time, around a median time,
    then they become asymptomatic or symptomatic, depending on their susceptibility.
    In both case, they are now infectious, but only symptomatic ones will be transferred to hospitals.

    The state of an individual will evolve
    depending of the susceptibility and the care provided (being in a hospital and well cared for)
    and eventually become worse.
    If a person survives long enough, he/she will become immunised or die in the opposite case.

    Hospitals receive a certain amount of resources each day and use them depending on the patients number and states.
    If a hospital does not have enough resources or doctors to treat all patients, the quality of care drops and all the patients are less well cared for.
    In such cases, other hospitals can send resources and doctors to those in need. Such transfers take one day.

## Lauching the simulation
    Launch it from simulation modifying the input arguments under vars_importantes.

## Results of the simulation
    Graphical results of how the disease evolves between individuals.
    Graphical representation of how full the hospitals are/... (to be completed)
    Graph of the hospitals resources over time.
    Representation of the population inside its environment and the states of the people.
    Graph of the proportion of different states in the population (cumulative).




## Contributors
    Amira Neumann, Arthur Valentin, Boris Kunz, Celine Le, Ester Simkova, Gregoire Molas, Tam Nguyen
