from individu import *
from vars_importantes import *
from hopital import *
from dataManager import *

region1 = 1
nbr_lits = 1
nbr_medecins = 1
basic_ressources = [5]
ressources_flow = [0]

hopital1 = Hopital(region1, nbr_lits, nbr_medecins, basic_ressources, ressources_flow)
hopital2 = Hopital(region1, nbr_lits, nbr_medecins, basic_ressources, ressources_flow)
hopital3 = Hopital(region1, nbr_lits, nbr_medecins, basic_ressources, ressources_flow)

routeur_tot = dict()
routeur_l1 = dict()
routeur_l2 = dict()
routeur_l3 = dict()

routeur_l1[hopital1] = 0
routeur_l1[hopital2] = 1
routeur_l1[hopital3] = 1

routeur_l2[hopital1] = 1
routeur_l2[hopital2] = 0
routeur_l2[hopital3] = 1

routeur_l3[hopital1] = 1
routeur_l3[hopital2] = 1
routeur_l3[hopital3] = 1

routeur_tot[hopital1] = routeur_l1
routeur_tot[hopital2] = routeur_l2
routeur_tot[hopital3] = routeur_l3

grille1 = "empty"

i1 = Individu(region1, grille1, (0,0), (3, 3),1)
i2 = Individu(region1, grille1, (0,0), (3, 3),1)
i3 = Individu(region1, grille1, (0,0), (3, 3),1)
i4 = Individu(region1, grille1, (0,0), (3, 3),1)
i5 = Individu(region1, grille1, (0,0), (3, 3),1)
i6 = Individu(region1, grille1, (0,0), (3, 3),1)
i7 = Individu(region1, grille1, (0,0), (3, 3),1)
i8 = Individu(region1, grille1, (0,0), (3, 3),1)
i9 = Individu(region1, grille1, (0,0), (3, 3),1)
i10 = Individu(region1, grille1, (0,0), (3, 3),1)

d = DataManager(set([i1, i2, i3, i4, i5, i6, i7, i8, i9, i10]), set([hopital1, hopital2, hopital3]), routeur_tot)

#print(d.routeur_hopitaux[hopital1][hopital2])

print("{},{},{}".format(hopital1.resources, hopital2.resources, hopital3.resources))
print(i1.etat)
i1.contamine()
for i in range(1,10):
    #♣print("{} {}".format(i1.etat, i1.temps))
    d.update()


print("{},{},{}".format(hopital1.resources, hopital2.resources, hopital3.resources))
print("{},{},{}".format(hopital1.alerte, hopital2.alerte, hopital3.alerte))