import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import operator

from vars_importantes import *
'''sauvegarde
def graphCumulatif( whole_count ,labels = [] ):
    plt.figure()
    if(len(labels) == 0): labels = [""] * len(whole_count)
    max_time= len(whole_count[0][0])
   
    for i in range(len(whole_count)):
        tmp=list(np.zeros(max_time))
        for j in range(len(whole_count)-i):
            tmp= list(map(operator.add, tmp, list(whole_count[j] )))
        vec= np.linspace(0,max_time-1,max_time)
        plt.plot(vec,tmp[0],'black',label = labels[i] )
        plt.fill_between(vec,tmp[0])
    plt.legend()
'''
def graphCumulatif( whole_count ,labels = [],titre="" , x="",y="" ):
    plt.figure()
    plt.title(titre)
    plt.xlabel(x)
    plt.ylabel(y)
    
    if(len(labels) == 0): labels = [""] * len(whole_count)
    max_time= len(whole_count[0])
    print(max_time)
    for i in range(len(whole_count)):
        tmp=list(np.zeros(max_time))
        for j in range(len(whole_count)-i):
            tmp= list(map(operator.add, tmp, list(whole_count[j] )))
        vec= np.linspace(0,max_time-1,max_time)
        plt.plot(vec,tmp,'black')#,label = labels[i] )
        plt.fill_between(vec,tmp,label = labels[i] )
    plt.legend()
    plt.savefig("Graph_cumulatif.png", format="png", dpi=300)

def graphPointOne( data , titre ):
    vec = np.linspace( 0 , len(data) -1, len(data) )
    plt.plot( vec, data , 'o-' , label = titre )
    
    
def graphPointSeveral( data , labels = [] , titre = "" , x="",y="" ):
    
    if(len(labels) == 0): labels = [""] * len(data)
    
    plt.figure()
    plt.title(titre)
    plt.xlabel(x)
    plt.ylabel(y)
    
    for i in range(len(data)):
        graphPointOne( data[i] , labels[i] )
    plt.legend()
    plt.savefig("several_points.png", format="png", dpi=300)
'''
def graphOneRegionEtat( individus , region_name ):
    liste=list(filter(lambda x : x.region == region_name, individus))
    
    liste_etat = list(x.etat for x in liste)
    
    max_time= max( list(len(liste_etat[i]) for i in range(len(liste_etat)) ))
    ###completer mort
    
    sains=[]
    immunise=[]
    incub=[]
    asymp=[]
    sympf=[]
    sympm=[]
    sympg=[]
    morts=[]
    
    for i in range(liste_etat):
        sains.append(liste_etat[i].count(Etat.Sain))
        immunise.append(liste_etat[i].count(Etat.Immunise))
        incub.append(liste_etat[i].count(Etat.IIncub))
        asymp.append(liste_etat[i].count(Etat.IASymp))
        sympf.append(liste_etat[i].count(Etat.ISympF))
        sympm.append(liste_etat[i].count(Etat.ISympM))
        sympg.append(liste_etat[i].count(Etat.ISympG))
        morts.append(liste_etat[i].count(Etat.Mort))
    
    whole_count = [ sains , immunise , incub , asymp , sympf , sympm, sympg , morts ]
    
    graphCumulatif( whole_count )
 '''   