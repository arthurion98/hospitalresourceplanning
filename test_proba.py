from individu import *
from vars_importantes import *
from hopital import *
from dataManager import *
import numpy as np

Somme = [0,0,0,0]
Temps = 0 ;
NB = np.zeros([6,2])
etat_prec = Etat.IIncub ;
temps= np.zeros(6)
Temp=0
mort = 0
for u in range(1,1001):
    region1 = "Union Soviétique"
    nbr_lits = 1
    nbr_medecins = 1
    basic_ressources = [5]
    ressources_flow = [1]

    hopital1 = Hopital(region1, nbr_lits, nbr_medecins, basic_ressources, ressources_flow)
    hopital2 = Hopital(region1, nbr_lits, nbr_medecins, basic_ressources, ressources_flow)
    hopital3 = Hopital(region1, nbr_lits, nbr_medecins, basic_ressources, ressources_flow)

    routeur_tot = dict()
    routeur_l1 = dict()
    routeur_l2 = dict()
    routeur_l3 = dict()

    routeur_l1[hopital1] = 0
    routeur_l1[hopital2] = 1
    routeur_l1[hopital3] = 1

    routeur_l2[hopital1] = 1
    routeur_l2[hopital2] = 0
    routeur_l2[hopital3] = 1

    routeur_l3[hopital1] = 1
    routeur_l3[hopital2] = 1
    routeur_l3[hopital3] = 1

    routeur_tot[hopital1] = routeur_l1
    routeur_tot[hopital2] = routeur_l2
    routeur_tot[hopital3] = routeur_l3

    grille1 = "empty"

    i1 = Individu(grille1, (0,0), (3, 3))
    i2 = Individu(grille1, (0,0), (3, 3))
    i3 = Individu(grille1, (0,0), (3, 3))
    i4 = Individu(grille1, (0,0), (3, 3))
    i5 = Individu(grille1, (0,0), (3, 3))
    i6 = Individu(grille1, (0,0), (3, 3))
    i7 = Individu(grille1, (0,0), (3, 3))
    i8 = Individu(grille1, (0,0), (3, 3))
    i9 = Individu(grille1, (0,0), (3, 3))
    i10 = Individu(grille1, (0,0), (3, 3))

    d = DataManager(set([i1, i2, i3, i4, i5, i6, i7, i8, i9, i10]), set([hopital1, hopital2, hopital3]), routeur_tot)

    #print(d.routeur_hopitaux[hopital1][hopital2])
    '''
        Sain = 0
        Immunise = 1
        IIncub = 2
        IASymp = 3
        ISympF = 4
        ISympM = 5
        ISympG = 6
        Mort = 7
    '''

    #print('\n')
    i1.contamine()
    for i in range(1,45):
        d.update()
        #print(i1.etat)
        '''
        if i1.etat == Etat.Immunise:
            if etat_prec == Etat.ISympF:
                Temps[0] += temps
                NB[0] += 1
            elif etat_prec == Etat.ISympM:
                Temps[1] += temps
                NB[1] += 1
            elif etat_prec == Etat.ISympG:
                Temps[2] += temps
                NB[2] += 1
        elif i1.etat== Etat.IASymp and etat_prec == Etat.IIncub:
            Temps[3] += temps
            NB[3] += 1
        '''
        if i1.etat != etat_prec:
            if i1.etat == Etat.Immunise:
                if etat_prec==Etat.ISympF:
                    NB[1][0] += 1
                    temps[1] +=Temp
                elif etat_prec== Etat.ISympM:
                    NB[2][0] += 1
                    temps[2] += Temp
                elif etat_prec== Etat.ISympG:
                    NB[3][0] += 1
                    temps[3] += Temp
                elif etat_prec == Etat.IASymp:
                    temps[4] += Temp
                    NB[4][0] += 1
            elif i1.etat == Etat.ISympM:
                temps[1] += Temp
                if etat_prec == Etat.ISympF:
                    NB[1][1] += 1
            elif i1.etat == Etat.ISympG:
                temps[2] += Temp
                if etat_prec == Etat.ISympM:
                    NB[2][1] += 1
            elif i1.etat == Etat.Mort:
                temps[3] += Temp
                if etat_prec == Etat.ISympG:
                    NB[3][1] += 1
            elif etat_prec == Etat.IIncub:# = i1.etat =Etat.IASy ou Etat.ISF
                temps[0] += Temp
                if i1.etat == Etat.IASymp:
                    NB[0][0] += 1
            elif i1.etat == Etat.IIncub:
                temps[5] += Temp
                NB[5][0] +=1



        etat_prec=i1.etat
        Temp = i1.temps

    #print(i1.etat)
    if i1.etat == Etat.Mort:
        mort +=1


    etat_prec = Etat.IIncub #DEVRAIT sain
    Temp = 0 ;
    #Somme = []

Pourcent_passer_etat = [NB[1][1]/(NB[1][1]+NB[1][0]),NB[2][1]/(NB[2][1]+NB[2][0]),NB[3][1]/(NB[3][1]+NB[3][0])]
temps_moyen = [temps[0]/(NB[0][0]+NB[1][1]+NB[1][0]),temps[1]/(NB[1][1]+NB[1][0]),temps[2]/(NB[2][1]+NB[2][0]),temps[3]/(NB[3][1]+NB[3][0]), temps[4]/NB[4][0]] #, temps[5]/NB[5][0]]
Mort = mort/1000
print(np.around(Pourcent_passer_etat,3))
print(Mort)
print(np.round(temps_moyen,3))