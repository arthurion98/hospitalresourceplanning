import numpy as np
import random as rd
from individu import *
from vars_importantes import *

class Grille():
    
    def __init__(self, region, size, nb_individus, ratio_eloignement_normal, rayon_eloignement, proba_infection, size_individus=(3, 3)):
        """
        :param size: (tuple (width, height)) taille de la grille
        :param nb_individus: (int) nombres d'individus pour initialiser la grille
        :param size_individus: (float) taille des individus dans la grille
        """
        self.region = region
        self.size = size
        self.w = size[0]
        self.h = size[1]
        self.nb_individus = nb_individus
        self.size_individus = size_individus
        self.rect = (0, 0, self.w, self.h)
        self.ratio_eloignement_normal = ratio_eloignement_normal
        self.rayon_eloignement = rayon_eloignement
        self.proba_infection = proba_infection
        
        # individus qui composent la population de la grille
        self.individus = set()
        n = int(np.sqrt(nb_individus))
        for i in range(nb_individus):
            s = 0
            if rd.random() < proportion_succept:
                s = 1
            self.individus.add(Individu(self.region, self.size, (int((i % n) * self.w / n), int((i / (n ** 2)) * self.h)), size=self.size_individus, succept = s))
        #self.individus = np.array(self.individus)
        
        # distances entre individus
        #self.distances_x = list(np.zeros((nb_individus, nb_individus)))
        #self.distances_y = self.distances_x.copy()
        #self.distances = self.distances_x.copy()
        self.distances_dict = dict()
        self.distances_dict_x = dict()
        self.distances_dict_y = dict()
        self.compute_distances()
        #self.distances_x = np.array(self.distances_x)
        #self.distances_y = np.array(self.distances_y)
        #self.distances = np.array(self.distances)
        self.ind_infectieux = set()   # contient les indices (dans la liste d'individus) des individus infectieux
    
    
    def compute_distances(self):
        self.distances_dict = dict()
        self.distances_dict_x = dict()
        self.distances_dict_y = dict()
        for ind_i in self.individus:
            self.distances_dict[ind_i] = dict()
            self.distances_dict_x[ind_i] = dict()
            self.distances_dict_y[ind_i] = dict()
            for ind_j in self.individus:
                self.distances_dict[ind_i][ind_j] = np.sqrt((ind_i.x - ind_j.x) ** 2 + (ind_i.y - ind_j.y) ** 2)
                self.distances_dict_x[ind_i][ind_j] = ind_i.x - ind_j.x
                self.distances_dict_y[ind_i][ind_j] = ind_i.y - ind_j.y
        #for i in range(self.nb_individus):
        #    for j in range(self.nb_individus):
        #        self.distances[i][j] = np.sqrt((self.individus[i].x - self.individus[j].x) ** 2 + (self.individus[i].y - self.individus[j].y) ** 2)
        #        self.distances_x[i][j] = self.individus[i].x - self.individus[j].x
        #        self.distances_y[i][j] = self.individus[i].y - self.individus[j].y
    
    def find_neighbours(self, ind_i, rayon):
        neighbours = set()
        for ind_j in self.individus:
            if(self.distances_dict[ind_i][ind_j] < rayon):
                neighbours.add(ind_j)
        return neighbours
        #return np.argwhere(self.distances[i] < rayon)
    
    def update(self):
        #x_dir = (1 - self.ratio_eloignement_normal) * 2 * (np.random.random(self.nb_individus) - 0.5)
        #y_dir = (1 - self.ratio_eloignement_normal) * 2 * (np.random.random(self.nb_individus) - 0.5)
        
        self.compute_distances()
        
        # déplacement
        for ind_i in self.individus:
            if ind_i.etat != Etat.Mort:
                x_dir = (1 - self.ratio_eloignement_normal) * 2 * (np.random.random() - 0.5)
                y_dir = (1 - self.ratio_eloignement_normal) * 2 * (np.random.random() - 0.5)
                
                neighbours = self.find_neighbours(ind_i, self.rayon_eloignement)
                neighbours_x = []
                neighbours_y = []
                for ind_j in neighbours:
                    if ind_i != ind_j:
                        if(self.distances_dict_x[ind_i][ind_j] != 0):
                            neighbours_x.append(self.distances_dict_x[ind_i][ind_j])
                        if(self.distances_dict_y[ind_i][ind_j] != 0):
                            neighbours_y.append(self.distances_dict_y[ind_i][ind_j])
                force_x = self.ratio_eloignement_normal * min(np.sum(np.sign(neighbours_x) / (np.array(neighbours_x) ** 2)), max_force) / max_force
                force_y = self.ratio_eloignement_normal * min(np.sum(np.sign(neighbours_y) / (np.array(neighbours_y) ** 2)), max_force) / max_force
                ind_i.update_pos((ind_i.x + update_distance * (x_dir + force_x), ind_i.y + update_distance * (y_dir + force_y)))
         
        # recalcul des nouvelles distances
        self.compute_distances()
        
        # infection
        new_infectieux = set()
        self.ind_infectieux = set()
        
        for ind_i in self.individus:
            if ind_i.etat in infectieux:
                self.ind_infectieux.add(ind_i)
        
        for inf_i in self.ind_infectieux:
            neighbours = self.find_neighbours(inf_i, rayons_contagion[inf_i.etat])
            for neig in neighbours:
                if neig != inf_i and neig.etat == Etat.Sain and rd.random() < self.proba_infection:
                    neig.contamine()
        
        
        #to_remove = []
        #to_kill = []
        #for inf_i in self.infectieux:
        #    print("infectieux", inf_i.etat)
        #    if self.individus[i].etat == Etat.Mort:
        #        to_remove.append(i)
        #        to_kill.append(i)
        #    elif self.individus[i].etat == Etat.Immunise or self.individus[i].etat == Etat.Sain or self.individus[i].etat == Etat.IIncub:
        #        to_remove.append(i)
        #    else:
        #        print("infecté", self.individus[i].etat)
        #        indices = self.find_neighbours(i, rayons_contagion[self.individus[i].etat])
        #        probas = np.random.random(len(indices))
        #        nvx_indices = np.argwhere(probas < self.proba_infection)
        #        for j in nvx_indices:
        #            index = indices[j[0]][0]
        #            if i != index and self.individus[index].etat == Etat.Sain:
        #                self.individus[index].contamine()
        #                new_infectieux.add(index)
                
        #self.infectieux.update(new_infectieux)
        
        #for i in range(self.nb_individus):
        #    if self.individus[i].etat in infectieux and self.individus[i] not in self.infectieux:
        #        self.infectieux.update([i])
        
        #for i in to_remove:
        #    self.infectieux.remove(i)
        #for i in to_kill:
        #    self.remove_individu(self.individus[i])
    
    def contamine(self, nb):
        buffer = rd.sample(self.individus, nb)
        for ind in buffer:
            ind.contamine()
    
    def add_individu(self, ind):
        ind.inGrille = True
        self.individus.add(ind)
        x, y = np.random.random(2) * self.w
        ind.update_pos((x, y))
        self.nb_individus += 1
    
    def remove_individu(self, ind):
        if ind in self.individus:
            ind.inGrille = False
            self.individus.remove(ind)
            self.nb_individus -= 1
    
    def draw(self, win, position, size):
        pygame.draw.rect(win, (0, 0, 0), (position[0], position[1], self.rect[2] + 4 + 2 * self.size_individus[0], self.rect[3] + 4 + 2 * self.size_individus[1]))
        pygame.draw.rect(win, (255, 255, 255), (position[0] + 2, position[1] + 2, self.rect[2] + 2 * self.size_individus[0], self.rect[3] + 2 * self.size_individus[1]))
        for individu in self.individus:
            individu.draw(win, position)