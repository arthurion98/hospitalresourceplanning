import numpy as np
import operator
from individu import *
from vars_importantes import *

class Hopital():
    
    def __init__(self, region, beds, medecins, resources, oneRefill):
        #localisation -> adresse
        self.region = region
        #lits -> int
        self.beds = beds
        #medecin et medecin en transfert -> int
        self.medecins = medecins
        self.medTrans = 0
        #materiel et materiel en transfert -> tableau de int
        self.resources = resources
        self.resTrans = [0 for i in range(0, nbr_type_ressources)]
        #listes de patients
        self.patients = set()
        self.patientsTrans = set()
        #alerte de ce qui manques -> bool 
        self.alerte=False
        self.alerteMed=0
        self.alerteRes= list(np.zeros(len(self.resources)))
        
        self.consoHier=list(np.zeros(len(self.resources)))
        self.oneRefill=oneRefill
        
    def update(self):
        patient_to_remove = set()
        for patient in self.patients:
            if((patient.etat == Etat.Immunise) or (patient.etat == Etat.Mort)):
                patient_to_remove.add(patient)
        for patient in patient_to_remove:
            patient.eteSoigne = True
            patient.soigne = False 
            self.patients.remove(patient) 
        self.refill()
        self.transReset()
        self.alerteReset()
        self.consommation()
        self.soins_medicaux()
        self.urgence()
        
        for patient in self.patients:
            patient.soigne = True
            patient.eteSoigne = True 
            patient.bienSoigne = self.quality()
    
    
    def alerteReset(self):
        self.alerte=False
        self.alerteMed=0
        self.alerteRes= list(np.zeros(len(self.resources)))
    #modification des resources    
    ### les faible,moyen,graveRes sont des tableaux fix des resources neessaires pour le soins seelon les cas
    def consommation(self):
        self.consoHier=list(np.zeros(len(self.resources)))
        for patient in self.patients:
            etat = patient.etat
            ##pas de consommation si pas assez de resources
            ##voir l'ordre de priorité d'attribution de resources
            #failble
            if(etat == Etat.ISympF):
                tmp = list(map(operator.sub, self.resources, faibleRes ))
                if(any(n < 0 for n in tmp)):
                    for n in tmp:
                        if n < 0:
                            self.alerteRes[tmp.index(n)]+=abs(n)
                else: 
                    self.resources=tmp
                    self.consoHier=list(map( operator.add , self.consoHier , faibleRes))
            #moyen
            elif(etat == Etat.ISympM):
                tmp = list(map(operator.sub, self.resources, moyenRes ))
                if(any(n < 0 for n in tmp)):
                    for n in tmp:
                        if n < 0:
                            self.alerteRes[tmp.index(n)]+=abs(n)
                else: 
                    self.resources = tmp
                    self.consoHier = list(map( operator.add , self.consoHier , moyenRes))
            #grave
            elif(etat == Etat.ISympG):
                tmp = list(map(operator.sub, self.resources, graveRes ))
                if any(n < 0 for n in tmp):
                    for n in tmp:
                        if n < 0:
                            self.alerteRes[tmp.index(n)] += abs(n)
                else: 
                    self.resources = tmp
                    self.consoHier = list(map( operator.add , self.consoHier , graveRes))
            
    def soins_medicaux(self):
        result = self.medecins - int(len(self.patients) / med_par_patient)
        if(result < 0):
            self.alerteMed=abs(result)
    
    #les choses en transition passe à 0 (transfert fait)    
    def transReset(self): 
        self.medecins += self.medTrans
        self.medTrans = 0
        
        for i in range(len(self.resources)):
            self.resources[i] += self.resTrans[i]
            self.resTrans[i] = 0
            
        self.patients.update(self.patientsTrans)
            
       # self.patients.update(self.patientsTrans): l'update se fait depuis simulation par chaque patient 
        self.patientsTrans = set()
        
    
    #cb de medecin je peux donner potentielement
    def cb_med_don(self):
        if self.alerteMed <= 0:
            result = self.medecins - int(len(self.patients)/med_par_patient)
            if(result > 0):
                return result
        return 0
    
    #idem resources
    def cb_res_don(self):
        if not(any(n > 0 for n in self.alerteRes)):
            dispo=list(map(operator.sub , self.resources, self.consoHier))
            for n in dispo:
                if(n < 0):
                    n = 0
            return dispo
        else:
            return [0 for i in range(0,nbr_type_ressources)]
        
    #true -> peut accepter un patient // false -> plein
    def acceptation(self):
        return ( self.beds > len(self.patients) )
    
    def urgence(self):
        if(self.alerteMed>0):
            #self.requiert_medecin()
            self.alerte=True
        if(any(n > 0 for n in self.alerteRes)):
            #self.requiert_resources()
            self.alerte=True
            
        if( not(self.alerteMed) and not(any(n > 0 for n in self.alerteRes) )):
            self.alerte=False
    
    def requiert_medecin(self):
        simulation.wantMed(self,self.alerteMed)
        
    def requiert_resources(self):
        simulation.wantRes(self,self.alerteRes)
        
    def transfert_Medecin(self, nb):
        if(nb>0):
            self.medTrans+=int(nb)
        else: self.medecins+=int(nb)
        
    def transfert_Resources(self, tab):
        for i in tab:
            if( i < 0 ): self.resources[tab.index(i)]+=int(i)
            elif( i > 0 ): self.resTrans[tab.index(i)]+=int(i)
        
    def transfert_Patient(self, patient):
        self.patientsTrans.add(patient)
        
    def quality(self):
        return not self.alerte

    def refill(self):
        self.resources=list(map(operator.add , self.resources , self.oneRefill))
        
    def nb_immunises(self):
        count = 0
        for patient in self.patients:
            if patient.etat == Etat.Immunise:
                count += 1
        return count
    def nb_morts(self):
        count=0
        for patient in self.patients:
            if patient.etat == Etat.Mort:
                count += 1
        return count
    def nb_patients(self):
        return len(self.patients)
    def nb_patients_par_catego(self):
        count_S_B=np.zeros(8) #nb Succeptibles in bad hospitals
        count_N_B=np.zeros(8) #nb non succeptibles in bad hospitals
        count_S_G=np.zeros(8) #nb Succeptibles in good hospitals
        count_N_G=np.zeros(8) #nb non succeptibles in good hospitals
        for patient in self.patients:
            if patient.succept:
                if patient.bienSoigne:
                    count_S_G[patient.etat.value] += 1
                else :
                    count_S_B[patient.etat.value] += 1
            else :
                if patient.bienSoigne:
                    count_N_G[patient.etat.value] += 1
                else :
                    count_N_B[patient.etat.value] += 1
        return [count_S_B, count_S_G,count_N_B,count_N_G]
        