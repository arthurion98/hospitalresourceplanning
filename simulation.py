import pygame 
import time
import random
import matplotlib.pyplot as plt
from win32api import GetSystemMetrics
import os
from inspect import getsourcefile
from os.path import abspath

path = abspath(getsourcefile(lambda:0))
path = path[0 : -len('simulation.py')]
os.chdir(path)

from grille import *
from individu import *
from vars_importantes import *
from hopital import *
from dataManager import *
from ville import *

# Graphic Parameters
width_default = 1000
height_default = 600
width = GetSystemMetrics(0)
height = GetSystemMetrics(1)
pygame.init()

# Graphics
def redrawWindow(win, villes, positions, size, defuns, hopitaux):
    win.fill((255, 255, 255))
    for i in range(len(villes)):
        villes[i].draw(win, positions[i], size)
        font = pygame.font.Font('regular.ttf', 10)
        text = font.render('Nombre de morts : {}'.format(len(list(filter(lambda x : x.region == i + 1, defuns)))), True, (0, 0, 0), (255, 255, 255))
        textRect = text.get_rect()
        textRect.x = positions[i][0]
        textRect.y = positions[i][1] + size[1] + 16
        win.blit(text, textRect)
        
        hosp = list(filter(lambda x : x.region == i + 1, hopitaux))
        text2 = font.render('Nombre de patients dans les hopitaux : {}'.format(sum([h.nb_patients() for h in hosp])), True, (0, 0, 0), (255, 255, 255))
        textRect2 = text.get_rect()
        textRect2.x = positions[i][0]
        textRect2.y = positions[i][1] + size[1] + 32
        win.blit(text2, textRect2)
    
    pygame.display.update()

# Main
def main():
    run = True
    # win = pygame.display.set_mode((width, height), pygame.FULLSCREEN)
    #win = pygame.display.set_mode((width_default, height_default), pygame.RESIZABLE)
    #pygame.display.set_caption("Simulation")
    
    # 5 régions
    regions = np.arange(1, 6)
    #regions = [1]
    
    villes = list(np.zeros(len(regions)))
    population = [100, 140, 225, 25, 10]
    for i in range(len(villes)):
        villes[i] = Ville(regions[i], population[i])
    villes = np.array(villes)
    
    individus = list(villes[0].grille.individus)
    for i in range(1, len(villes)):
        individus += list(villes[i].grille.individus)
    individus = set(individus)
    
    d = DataManager(individus, set(), dict(), villes)
    d.initialisation()
    
    for i in range(len(villes)):
        villes[i].contamine(1)
    
    # paramètres pour dessiner les villes
    positions = [(0, 0), (0, 300), (300, 0), (300, 300), (600, 0)]
    size = size_gril

    while run:
        
        #keys = pygame.key.get_pressed()
        #if keys[pygame.K_ESCAPE]:
        #    win = pygame.display.set_mode((width_default, height_default), pygame.RESIZABLE)
        #if keys[pygame.K_f]:
        #    win = pygame.display.set_mode((width, height), pygame.FULLSCREEN)
            
        #for event in pygame.event.get():
        #    if event.type == pygame.VIDEORESIZE:
        #        w, h = event.size
        #        # redraw win in new size
        #        win = pygame.display.set_mode((w, h), pygame.RESIZABLE)
        #    if event.type == pygame.QUIT:
        #        run = False

        # update grille
        #probas_switch_grid = np.random.random(len(villes))
        for i in range(len(villes)):
            villes[i].update()
        #    # un individu peut switcher d'une région à l'autre
        #    if proba_stay < probas_switch_grid[i] * len(villes[i].grille.individus):
        #        region = np.random.randint(len(villes) - 1)
        #        if region == i:
        #            region = len(villes) - 1
        #        individu = np.random.choice(list(villes[i].grille.individus))
        #        individu.region = region + 1
        #        villes[region].add_individu(individu)
        #        villes[i].remove_individu(individu)
            
        # update simulation
        d.update()
        if d.flag:
            run = False

        # update graphism
        #redrawWindow(win, villes, positions, size, d.defuns, d.hopitaux)

        # frame delta time
        #time.sleep(.2)

    #pygame.quit()
    
    #affichage
    d.affichage_individus()

#MAIN=========================================================================

main()
plt.show()