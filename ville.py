from hopital import *
from dataManager import *
from grille import *
from individu import *
from vars_importantes import *
import random
'''
region1 = "Union Soviétique"
nb_lits = 1
nb_medics = 1
basic_ressources = [50]
ressources_flow = [10]
'''
class Ville():
    
    def __init__(self, region, nb_individus):
        """
        :param region: (int) région dans laquelle se trouve la ville
        :param nb_hopitaux: (int) nombre d'hopitaux dans la ville
        :param nb_individus: (int) nombre d'individus dans la ville
        """
        self.region = region
        self.grille = Grille(region, size_gril, nb_individus, ratio_eloi, rayon_eloi, proba_inf)
        self.nb_individus = nb_individus
        '''
        self.nb_hopitaux = nb_hopitaux
        
        self.hopitaux = list(np.zeros(nb_hopitaux))
        for i in range(nb_hopitaux):
            self.hopitaux[i] = Hopital(region, nb_lits, nb_medics, basic_ressources, ressources_flow)
        self.hopitaux = np.array(self.hopitaux)
        '''
        #self.hopitaux=hopitaux
        
        #self.routeur_tot = dict()
        #routeur = dict()
        #for i in range(len(self.hopitaux)):
        #    routeur[self.hopitaux[i]] = 1
        #for i in range(len(self.hopitaux)):
        #    routeur[self.hopitaux[i]] = routeur.copy()
        
        #self.data_manager = DataManager(set(self.grille.individus), set(self.hopitaux), self.routeur_tot)
    
    
    #def addHopital( new_hopital ):
    #    self.hopitaux.append(new_hopital)
    
    def update(self):
        self.grille.update()
        #self.data_manager.update()
        self.nb_individus = self.grille.nb_individus
    
    def contamine(self, nb):
        self.grille.contamine(nb)
    
    def draw(self, win, position, size):
        self.grille.draw(win, position, size)
    
    def add_individu(self, individu):
        self.grille.add_individu(individu)
    
    def remove_individu(self, individu):
        self.grille.remove_individu(individu)












