from enum import Enum

class Etat(Enum):
    Sain = 0
    Immunise = 1
    IIncub = 2
    IASymp = 3
    ISympF = 4
    ISympM = 5
    ISympG = 6
    Mort = 7

class Succept(Enum):
    Pas_risque = 0
    Risque = 1


porteur = (Etat.IIncub, Etat.IASymp, Etat.ISympF, Etat.ISympM, Etat.ISympG)
infectieux = (Etat.IASymp, Etat.ISympF, Etat.ISympM, Etat.ISympG)
malade = (Etat.ISympM, Etat.ISympG)

rayons_contagion = {
    Etat.IASymp : 20,
    Etat.ISympF : 30,
    Etat.ISympM : 40,
    Etat.ISympG : 50
}

couleurs = {
    "noir" : (0, 0, 0),
    "blanc" : (255, 255, 255),
    "gris clair" : (200, 200, 200),
    "gris foncé" : (100, 100, 100),
    "rouge" : (200, 0, 0),
    "vert" : (0, 200, 0),
    "bleu" : (0, 0, 200),
    "jaune" : (200,200,0),
    "orange" : (255, 140, 0)
}

etats_abr = {
    Etat.Sain : "Sain",
    Etat.IIncub : "IIncub",
    Etat.IASymp : "IASymp",
    Etat.ISympF : "ISymp",
    Etat.ISympM : "ISymp",
    Etat.ISympG : "ISymp",
    Etat.Mort : "Mort",
    Etat.Immunise : "Immunise"
}

couleurs_etats = {
    "Sain" : "vert",
    "IIncub" : "jaune",
    "IASymp" : "orange",
    "ISymp" : "rouge",
    "Mort" : "noir",
    "Immunise" : "gris clair"
}

probabilites = [[[0.03,0.4,0.034,0.035],[0.1,0.05,0.04,0.01],[0.1,0.05,0.04,0.01]],[[0.03,0.4,0.034,0.035],[0.1,0.05,0.04,0.01],[0.1,0.05,0.04,0.01]]]
# probas pas vrmt changé
# NH : nc et c : [0.03,0.01,0.014,0.017]
lam= [[[0.06,0.05,0.03,0.05,0.02],[0.03,0.04,0.015,0.02,0.01],[0.03,0.04,0.015,0.02,0.01]],[[0.08,0.02,0.05,0.07,0.01],[0.04,0.04,0.02,0.03,0.01],[0.035,0.04,0.015,0.019,0.009]]]
# NH: non succeptibles : [0.06,0.05,0.03,0.05,0.02] nb moyen jours pr cette transition [4,5,5,4,6]
# H mauvais: non succeptibles :[0.03,0.05,0.015,0.02,0.01] nb moyen jours pr cette transition [6,5,7,6,11]
# H bon: non succeptibles  :[0.02,0.05,0.008=> 0.01,0.01=>0.013,0.008] nb moyen jours pr cette transition [8,5,9,8,10]
# NH : succeptibles :[0.08,0.02,0.05,0.07,0.01] nb moyen jours pr cette transition [3.5,8,4,3,10]
# H mauvais: succeptibles :[0.04,0.05,0.02,0.03,0.01] nb moyen jours pr cette transition [5,5,6,5,10]
# H bon: succeptibles  :[0.025=>27,0.05,0.01=>13,0.01=>14,0.009] nb moyen jours pr cette transition [7,5,7,7,9]

nbr_lits_hopital = 5
nbr_medecins_hopital = 4
basic_ressources_hopital = [50]
ressources_flow_hopital = [1]

update_distance = 5
max_force = 10
ratio_eloi = 0.5
rayon_eloi = 10
proba_inf = 0.5
size_gril = (200,200)

proportion_succept = 0.2

proba_stay = .999

#valeur test
nbr_type_ressources = 1
faibleRes = [1]
moyenRes = [2]
graveRes = [3]
med_par_patient = 1